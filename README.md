# yii2-alexgallery

![image title](https://i.imgur.com/LZgKoNp.jpg)


**Create a table** 

Run the migration
```
yii migrate --migrationPath=@vendor/alexs/yii2-alexgallery/src/migrations
```
or execute SQL-code
```
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `pos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT;

ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
```

**Create a model**
```php
<?php
namespace app\models;

class Gallery extends \alexs\alexgallery\models\Gallery
{

}
```

**Create a controller**
```php
<?php
namespace app\modules\admin\controllers;

class GalleryController extends \alexs\alexgallery\controllers\GalleryController
{
    public $enableCsrfValidation = false; // remove if a widget used inside active form

    public function actionIndex() {
        return $this->render('gallery', [

        ]);
    }
}
```

**Init the widget in a template of your gallery**
```php
AlexGallery::widget([
    'Model'=>new Gallery,
    'upload_url'=>'/admin/gallery/upload',
    'delete_url'=>'/admin/gallery/delete',
    'sort_url'=>'/admin/gallery/sort',
])
```

**If you need a relation**

Add a foreign key **article_id** to gallery table in the database

```php
<?php
namespace app\models;

class ArticleGallery extends \alexs\alexgallery\models\Gallery
{
    /**
     * @inheritDoc
     */
    public static function getGalleryRelationAttribute() {
        return 'article_id';
    }
}
```
```php
<?php
namespace app\modules\admin\controllers;

class GalleryController extends \alexs\alexgallery\controllers\GalleryController
{
    public function actionIndex() {
        return $this->render('gallery', [

        ]);
    }
}
```
```php
<?php
AlexGallery::widget([
    'Model'=>new ArticleGallery,
    'RelationModel'=>$ArticleModel,
    'upload_url'=>'/admin/article-gallery/upload',
    'delete_url'=>'/admin/article-gallery/delete',
    'sort_url'=>'/admin/article-gallery/sort'
])
```