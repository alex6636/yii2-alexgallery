<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   10-June-19
 */

namespace alexs\alexgallery\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;

class GalleryUploadTest extends DatabaseTableTestCase
{
    public function testUpload() {

    }

    protected function setUp()
    {
        parent::setUp();
        \Yii::$app->controllerNamespace = 'alexs\\alexgallery\\tests\\controllers';
    }

    protected function getTableName()
    {
        return 'gallery';
    }

    protected function getTableColumns()
    {
        return [
            'id' => 'pk',
            'image'=> 'string DEFAULT NULL',
            'pos'=>'int DEFAULT 0',
        ];
    }
}