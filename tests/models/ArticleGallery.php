<?php
namespace alexs\alexgallery\tests\models;
use alexs\alexgallery\models\Gallery;

/**
 * @property int $article_id
 */

class ArticleGallery extends Gallery
{
    public function getRelationField() {
        return 'article_id';
    }
}
