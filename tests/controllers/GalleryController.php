<?php
namespace alexs\alexgallery\tests\controllers;

class GalleryController extends \alexs\alexgallery\controllers\GalleryController
{
    public $actions_namespace = '\alexs\alexgallery\actions';
    public $model_namespace = '\alexs\alexgallery\models';
}
