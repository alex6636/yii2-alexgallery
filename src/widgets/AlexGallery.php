<?php
namespace alexs\alexgallery\widgets;
use alexs\alexgallery\models\Gallery;
use yii\base\Widget;
use yii\db\ActiveRecord;
use yii\helpers\Html;

class AlexGallery extends Widget
{
    /** @var Gallery $Model */
    public $Model;
    /** @var ActiveRecord|null */
    public $RelationModel;
    public $upload_url;
    public $delete_url;
    public $sort_url;
    public $limit = null;
    public $delete_confirmation = null;
    protected static $gallery_index = 0;

    public function run() {
        ++ static::$gallery_index;
        $this->registerGalleryJs();
        return $this->render($this->gerViewName(), [
            'Model'=>$this->Model,
            'gallery_id'=>$this->getGalleryId(),
            'upload_input_params'=>$this->getUploadInputParams(),
        ]);
    }

    /**
     * @return string
     */
    public function gerViewName() {
        return 'alex-gallery';
    }

    /**
     * @return array
     */
    public function getUploadInputParams() {
        return [
            'class'=>'alex-gallery-upload-image',
            'multiple'=>'multiple',
            'accept'=>implode(', ', array_map(function($val){return ".$val";}, $this->Model::$gallery_image_ext)),
            'capture'=>'camera',
        ];
    }

    public function registerGalleryJs() {
        $js_images = '';
        $uploaded_gallery = $this->Model::getGalleryUploadedImages($this->RelationModel);
        if (!empty($uploaded_gallery)) {
            foreach ($uploaded_gallery as $Gallery) {
                $js_image_obj = "{id:" . $Gallery->id . ", src:'" . $Gallery->getUploadedFileURL('image', $Gallery::$gallery_thumb_subdir) . "', source:'" . $Gallery->getUploadedFileURL('image') . "'}";
                $js_images .= "gallery.insertImagePreview(" . $js_image_obj . ");\n";
            }
        }
        $js = "$(function() {
            var gallery = new alexGallery(
                '" . $this->getGalleryId() . "',
                '" . $this->getInputName() . "',
                '" . $this->upload_url . "',
                '" . $this->delete_url . "',
                '" . $this->sort_url . "',
                " . $this->getRelationUploadParamsJs() . ",
                " . $this->getRelationInputName() . ",
                " . ($this->limit ? $this->limit : "null") . ",
                " . ($this->delete_confirmation ? json_encode($this->delete_confirmation) : "null") . "
            );
            " . $js_images . "
        });";
        $this->view->registerJs($js, $this->view::POS_READY);
    }

    public function getGalleryId() {
        return 'alex-gallery-' . static::$gallery_index;
    }

    public function getInputName() {
        return Html::getInputName($this->Model, $this->Model::getGalleryImageAttribute());
    }

    public function getRelationUploadParamsJs() {
        if (!$this->RelationModel) {
            return 'null';
        }
        if (($relation_attr = $this->Model::getGalleryRelationAttribute()) && ($relation_val = $this->RelationModel->getPrimaryKey())) {
            return json_encode([Html::getInputName($this->Model, $relation_attr)=>$relation_val]);
        }
        return 'null';
    }

    public function getRelationInputName() {
        if (!$this->RelationModel) {
            return 'null';
        }
        return "'" . Html::getInputName($this->RelationModel, 'gallery_' . $this->Model::getGalleryImageAttribute()) . "[]'";
    }
}
