<?php
use alexs\alexgallery\assets\AlexGalleryAsset;
use alexs\alexgallery\models\Gallery;
use yii\helpers\Html;

/** @var Gallery $Model */
/** @var array $upload_input_params */
/** @var string $gallery_id */

AlexGalleryAsset::register($this);
?>
<div id="<?=$gallery_id?>" class="alex-gallery">
    <div class="alex-gallery-upload-box">
        <div class="inner">
            <div class="upload">
                <?=Html::fileInput(null, null, $upload_input_params)?>
                <div class="files">
                    <?=Yii::t('app', 'Drag photos here or click to select files')?>
                    <div class="format"><?=implode(', ', $Model::$gallery_image_ext)?> <?=Yii::t('app', 'maximum {max_size} Mb', ['max_size'=>$Model::$gallery_image_max_size_mb])?></div>
                </div>
            </div>
            <div class="preview"></div>
        </div>
    </div>
</div>