<?php
namespace alexs\alexgallery\models;
use alexs\yii2crud\models\CrudModelSorted;
use alexs\yii2fileable\Imageable;
use yii\db\ActiveRecord;

/**
 * @property string $image
 */

class Gallery extends CrudModelSorted
{
    public static $gallery_image_ext = ['jpg', 'jpeg', 'png', 'gif'];
    public static $gallery_image_max_size_mb = 10;
    public static $gallery_thumb_subdir = 'thumb';
    public static $gallery_thumb_width = 100;
    public static $gallery_thumb_height = 100;

    /**
     * @return string
     */
    public static function getGalleryImageAttribute() {
        return 'image';
    }

    /**
     * @return string|null
     */
    public static function getGalleryRelationAttribute() {
        return null;
    }

    /**
     * @return array
     */
    public function getGalleryThumbnails() {
        return [
            ['subdir'=>static::$gallery_thumb_subdir, 'width'=>static::$gallery_thumb_width, 'height'=>static::$gallery_thumb_height],
        ];
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return [
            'gallery_imageable'=>[
                'class'=>Imageable::class,
                'upload_dir'=>static::getUploadDir(),
                'attribute'=>static::getGalleryImageAttribute(),
                'thumbnails'=>$this->getGalleryThumbnails(),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules() {
        $rules = [
            [static::getGalleryImageAttribute(), 'required'],
            [static::getGalleryImageAttribute(), 'image', 'extensions'=>static::$gallery_image_ext, 'maxSize'=>static::$gallery_image_max_size_mb * 1024 * 1024]
        ];
        if ($relation_attr = $this->getGalleryRelationAttribute()) {
            $rules[] = [$relation_attr, 'integer'];
        }
        return $rules;
    }

    /**
     * @param ActiveRecord|null $RelationModel
     * @return Gallery[]
     */
    public static function getGalleryUploadedImages(ActiveRecord $RelationModel = null) {
        $ActiveQuery = static::find();
        if ($relation_attr = static::getGalleryRelationAttribute()) {
            $ActiveQuery->where([$relation_attr=>$RelationModel->getPrimaryKey()]);
        }
        $ActiveQuery->orderBy(static::$sort_attribute);
        return $ActiveQuery->all();
    }

    /**
     * @return int
     */
    public function getSortMaxValue() {
        $ActiveQuery = static::find()->select('MAX(' . static::$sort_attribute . ')');
        if ($relation_attr = static::getGalleryRelationAttribute()) {
            $ActiveQuery->where([$relation_attr=>$this->$relation_attr]);
        }
        return (int) $ActiveQuery->scalar();
    }
}