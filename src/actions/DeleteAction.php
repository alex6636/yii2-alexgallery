<?php
namespace alexs\alexgallery\actions;
use alexs\alexgallery\controllers\GalleryController;
use alexs\alexgallery\models\Gallery;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DeleteAction extends Action
{
    public $response_format = Response::FORMAT_JSON;

    /**
     * @param int $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function run($id) {
        \Yii::$app->response->format = $this->response_format;
        if (!$Gallery = static::findItem($id)) {
            throw new NotFoundHttpException;
        }
        $Gallery->delete();
        return ['success'=>1];
    }

    /**
     * @return Gallery|null
     */
    public function findItem($id) {
        /** @var GalleryController $controller */
        $controller = $this->controller;
        /** @var Gallery $model_name */
        $model_name = $controller->getModelName();
        return $model_name::findOne($id);
    }
}