<?php
namespace alexs\alexgallery\actions;
use alexs\alexgallery\controllers\GalleryController;
use alexs\alexgallery\models\Gallery;
use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\Response;

class SortAction extends Action
{
    public $response_format = Response::FORMAT_JSON;

    /**
     * @return mixed
     */
    public function run() {
        $ids = $this->getIds();
        if (empty($ids)) {
            throw new BadRequestHttpException('Please provide the data');
        }
        $pos = 1;
        foreach ($ids as $id) {
            /** @var Gallery $Gallery */
            if ($Gallery = static::findItem($id)) {
                $Gallery->{$Gallery::$sort_attribute} = $pos;
                $Gallery->save(false);
                $pos ++;
            }
        }
        \Yii::$app->response->format = $this->response_format;
        return ['success'=>1];
    }

    /**
     * @return array
     */
    public function getIds() {
        return array_map('intval', (array) \Yii::$app->request->post('ids'));
    }

    /**
     * @return Gallery|null
     */
    public function findItem($id) {
        /** @var GalleryController $controller */
        $controller = $this->controller;
        /** @var Gallery $model_name */
        $model_name = $controller->getModelName();
        return $model_name::findOne($id);
    }
}