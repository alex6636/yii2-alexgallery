<?php
namespace alexs\alexgallery\actions;
use alexs\alexgallery\controllers\GalleryController;
use alexs\alexgallery\models\Gallery;
use yii\base\Action;
use yii\web\Response;

class UploadAction extends Action
{
    public $response_format = Response::FORMAT_JSON;

    /**
     * @return mixed
     */
    public function run() {
        $result = [];
        /** @var GalleryController $controller */
        $controller = $this->controller;
        $model_name = $controller->getModelName();
        /** @var Gallery $Gallery */
        $Gallery = new $model_name;
        $Gallery->load(\Yii::$app->request->post());
        if ($Gallery->validate()) {
            $Gallery->save(false);
            $result['success'] = 1;
            $result['image'] = [
                'id'=>$Gallery->id,
                'src'=>$Gallery->getUploadedFileURL('image', $Gallery::$gallery_thumb_subdir),
                'source'=>$Gallery->getUploadedFileURL('image'),
                'pos'=>$Gallery->{$Gallery::$sort_attribute},
            ];
        } else {
            $result['errors'] = $Gallery->getErrorSummary(true);
        }
        \Yii::$app->response->format = $this->response_format;
        return $result;
    }
}