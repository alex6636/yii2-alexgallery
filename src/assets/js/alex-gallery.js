/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   2019-2023
 */

class alexGallery
{
    'use strict';

    thumb_width = 100;
    thumb_height = 100;
    delete_confirmation = null;

    /**
     * $("body").on('alexGalleryItemUploaded', function () {
     *     // ...
     * });
     */
    eventItemUploaded = 'alexGalleryItemUploaded';

    /**
     * $("body").on('alexGalleryItemDeleted', function () {
     *     // ...
     * });
     */
    eventItemDeleted = 'alexGalleryItemDeleted';

    /**
     * $("body").on('alexGalleryItemSorted', function () {
     *     // ...
     * });
     */
    eventItemSorted = 'alexGalleryItemSorted';

    /**
     * $("body").on('alexGalleryItemInserted', function () {
     *     // ...
     * });
     */
    eventItemInserted = 'alexGalleryItemInserted';

    constructor(
        gallery_id,
        file_upload_input_name = 'Gallery[image]',
        upload_url = '/gallery/upload',
        delete_url = '/gallery/delete',
        sort_url   = '/gallery/sort',
        upload_params = {},
        relation_input_name = null,
        limit = null,
        delete_confirmation = null
    ) {
        this.gallery_id = gallery_id;
        this.file_upload_input_name = file_upload_input_name;
        this.upload_url = upload_url;
        this.delete_url = delete_url;
        this.sort_url = sort_url;
        this.upload_params = upload_params;
        this.relation_input_name = relation_input_name;
        this.limit = limit;
        this.delete_confirmation = delete_confirmation;
        this.initEvents();
    }

    initEvents() {
        var self = this;
        $(document).on('change', '#' +  self.gallery_id + ' .alex-gallery-upload-image', function() {
            self.uploadImage();
        });
        $(document).on('click', '#' +  self.gallery_id + ' .preview .button_delete', function(event) {
            var $delete = $(this), $preview_item = $delete.closest('.item'), id = $preview_item.data('id');
            event.preventDefault();
            if (self.delete_confirmation !== null) {
                if (!confirm(self.delete_confirmation)) {
                    return;
                }
            }
            $preview_item.addClass('loading');
            $.ajax({
                url: self.delete_url,
                data: {id: id},
                dataType: 'json'
            }).done(function(result) {
                $preview_item.remove();
                $('body').trigger(self.eventItemDeleted, [$preview_item, result]);
            }).fail(function() {
                self.displayError('Something went wrong');
            });
        });
        if (typeof Sortable !== 'undefined') {
            new Sortable($('#' +  self.gallery_id + ' .preview')[0], {
                animation: 150,
                onSort: function () {
                    self.onSort();
                }
            });
        }
    }

    getFileInput() {
        return $( '#' +  this.gallery_id + ' .alex-gallery-upload-image');
    }

    getPreviewContainer() {
        return $( '#' +  this.gallery_id + ' .preview');
    }

    getUploadedItems() {
        var $preview_container = this.getPreviewContainer();
        return $preview_container.find('.item');
    }

    getCountUploadedImages() {
        var $items = this.getUploadedItems();
        return $items.length;
    }

    isFinishedLoading() {
        var $items = this.getUploadedItems();
        return !$items.hasClass('loading');
    }

    resortUploadedItemsByPos() {
        var $preview_container = this.getPreviewContainer();
        var $items = this.getUploadedItems();
        $items.sort(function(a, b) {
            return $(a).data('pos') - $(b).data('pos');
        }).appendTo($preview_container);
    }

    /**
     * @param array|string error
     */
    displayError(error) {
        var msg = error.isArray ? error.join("\r\n") : error;
        alert(msg);
    }

    uploadImage() {
        var $file_input = this.getFileInput();
        if (!$file_input[0].files || !$file_input[0].files[0]) {
            return;
        }
        if ((this.limit !== null) && (this.limit === this.getCountUploadedImages())) {
            return;
        }
        var self = this;
        for (var i = 0; i < $file_input[0].files.length; i ++) {
            (function() {
                var $preview = self.createImagePreview();
                var formData = new FormData();
                formData.append(self.file_upload_input_name, $file_input[0].files[i]);
                for (var name in self.upload_params) {
                    formData.append(name, self.upload_params[name]);
                }
                $.ajax({
                    url: self.upload_url,
                    method: 'POST',
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false
                }).done(function(result) {
                    if (result.success !== undefined) {
                        self.insertImagePreview(result.image, $preview);
                        if (self.isFinishedLoading()) {
                            self.resortUploadedItemsByPos();
                            $('body').trigger(self.eventItemUploaded, [$preview, result]);
                        }
                    } else if (result.errors) {
                        self.displayError(result.errors);
                        $preview.remove();
                    }
                }).fail(function() {
                    self.displayError('Something went wrong');
                    $preview.remove();
                });
            })();
        }
        $file_input.val('');
    }

    createImagePreview() {
        var $preview_container = this.getPreviewContainer();
        var $preview_item = $('<div class="item loading"></div>');
        $preview_container.append($preview_item);
        return $preview_item;
    }

    /**
     * @param object image {id: 1, src: 'thumb/xyz.jpg', source: 'xyz.jpg', pos: 1}
     * @param object|undefined $preview_item
     */
    insertImagePreview(image, $preview_item) {
        let $hiddenInput = $('<input type="hidden"/>');
        $hiddenInput.val(image.id);

        if (this.relation_input_name !== null) {
            $hiddenInput.attr('name', this.relation_input_name);
        }

        if ($preview_item === undefined) {
            $preview_item = this.createImagePreview();
        }
        $preview_item.removeClass('loading');
        $preview_item.data('id', image.id);
        $preview_item.data('pos', image.pos);
        $preview_item.css('width', this.thumb_width + 'px');
        $preview_item.css('height', this.thumb_height + 'px');
        $preview_item.css('background-image', 'url(' + image.src + ')');
        $preview_item.append($hiddenInput);
        this.insertPreviewButtons(image, $preview_item);
        $('body').trigger(this.eventItemInserted, [$preview_item, image]);
    }

    insertPreviewButtons(image, $preview_item) {
        var button_preview = '<a class="button button_preview" href="' + image.source + '" target="_blank"></a>';
        var button_delete = '<div class="button button_delete"></div>';
        $preview_item.append('<div class="buttons">' + button_preview + button_delete + '</div>');
    }

    onSort() {
        var $items = this.getUploadedItems();
        var formData = new FormData();
        var self = this;
        $items.each(function() {
            formData.append('ids[]', $(this).data('id'));
        });
        $items.addClass('loading');
        $.ajax({
            url: this.sort_url,
            method: 'POST',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function(result) {
            $items.removeClass('loading');
            $('body').trigger(self.eventItemSorted, [result]);
        }).fail(function() {
            self.displayError('Something went wrong');
            $items.removeClass('loading');
        });
    }
}