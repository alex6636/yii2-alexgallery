<?php
namespace alexs\alexgallery\assets;
use yii\web\AssetBundle;

class AlexGalleryAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@vendor/alexs/yii2-alexgallery/src/assets';

    /**
     * @inheritdoc
     */
    public $js = [
        'js/Sortable.min.js',
        'js/alex-gallery.js',
    ];

    /**
     * @inheritdoc
     */
    public $css = [
       'css/alex-gallery.css',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}