<?php
namespace alexs\alexgallery\controllers;
use alexs\yii2crud\controllers\CrudController;

class GalleryController extends CrudController
{
    public function actions() {
        return [
            'upload'=>'\alexs\alexgallery\actions\UploadAction',
            'delete'=>'\alexs\alexgallery\actions\DeleteAction',
            'sort'=>'\alexs\alexgallery\actions\SortAction',
        ];
    }
}
